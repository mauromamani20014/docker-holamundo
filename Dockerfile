FROM node:18

WORKDIR /home/app

COPY . .

EXPOSE 3000

CMD ["node", "index.js"]
